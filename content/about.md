---
title: "About"
date: 2022-10-09T11:15:38-03:00
draft: false
---

# Sobre mim

![Leticia](https://media-exp1.licdn.com/dms/image/C4D03AQEkklxekqV4VA/profile-displayphoto-shrink_800_800/0/1659135230692?e=2147483647&v=beta&t=SosEeFKZWLGVD6KHR7Hq6fVRRWccRD5EeYfF8oj3fFQ) 

### Nome: Leticia Miyuki Kimoto
### Idade: 20 anos
### Cidade: Mogi das Cruzes, SP
### Gêneros preferidos: Disco, New-wave
### Música preferida: Everybody Wants To Rule The World - Tears For Fears
### Álbum preferido: ABBA Gold
### Cantor preferido: Bruno Mars
### Cantora preferida: Celine Dion
### Banda preferida: Tears For Fears
### Grupo preferido: ABBA
### Musical preferido: Dear Evan Hansen