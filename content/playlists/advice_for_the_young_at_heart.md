---
title: "Advice_for_the_young_at_heart"
date: 2022-10-09T16:42:10-03:00
draft: false
---

# Advice For The Young At Heart

![SpendingMyTime](https://i.ytimg.com/vi/4jJuCi8EMh4/mqdefault.jpg?width=400) 

### Músicas sobre o amor. Algumas românticas, outras nem tanto...

## 1) [The Name Of The Game](https://www.youtube.com/watch?v=iJ90ZqH0PWI)
### ABBA

## 2) [All The Way](https://www.youtube.com/watch?v=sxps4ggfoy4&)
### Celine Dion & Frank Sinatra

## 3) [The Captain Of Her Heart](https://www.youtube.com/watch?v=YX-Ru1XkNZc)
### Double

## 4) [Spending My Time](https://www.youtube.com/watch?v=eG0IYV6G0I0)
### Roxette

## 5) [I Want To Know What Love Is](https://www.youtube.com/watch?v=4jA-_g_iSY0)
### Foreigner

## 6) [Head Over Heels](https://www.youtube.com/watch?v=CsHiG-43Fzg)
### Tears For Fears

## 7) [Heaven](https://www.youtube.com/watch?v=vf3zFvcKQkw)
### Bryan Adams

## 8) [How Deep Is Your Love](https://www.youtube.com/watch?v=MGzPlbwwnLY)
### Bee Gees

## 9) [Crash! Boom! Bang!](https://www.youtube.com/watch?v=aoXrd3_3fYI)
### Roxette

## 10) [Advice For The Young At Heart](https://www.youtube.com/watch?v=vBtzFOgKcv8)
### Tears For Fears