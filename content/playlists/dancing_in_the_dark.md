---
title: "Dancing_in_the_dark"
date: 2022-10-09T16:42:26-03:00
draft: false
---

# Dancing In The Dark

![Pulp Fiction](https://www.sentireascoltare.com/wp-content/uploads/2022/02/film-scene-3433_2-Pulp-Fiction.jpg?width=400) 

### Uma playlist que gosto de ouvir de madrugada quando estou só no meu quarto.

## 1) [Born to Run](https://www.youtube.com/watch?v=Wu4_zVxmufY)
### Bruce Springsteen

## 2) [Girl, You’ll Be A Woman Soon](https://www.youtube.com/watch?v=JAHA4Jh5jkw)
### Urge Overkill

## 3) [Holding Back The Years](https://www.youtube.com/watch?v=yG07WSu7Q9w)
### Simply Red

## 4) [Don’t Dream It’s Over](https://www.youtube.com/watch?v=J9gKyRmic20)
### Crowded House

## 5) [You Belong To The City](https://www.youtube.com/watch?v=8TToLgW7zuc)
### Glenn Frey

## 6) [Dancing With Tears In My Eyes](https://www.youtube.com/watch?v=PSQWUZ8a2Ho)
### Ultravox

## 7) [Puttin’ On The Ritz](https://www.youtube.com/watch?v=TMstTM01m28)
### Taco

## 8) [Enjoy The Silence](https://www.youtube.com/watch?v=aGSKrC7dGcY)
### Depeche Mode

## 9) [Take On The Run](https://www.youtube.com/watch?v=t7Csc6l4QLs)
### REO Speedwagon

## 10) [Dancing In The Dark](https://www.youtube.com/watch?v=8cNS_Mecbw8)
### Bruce Springsteen