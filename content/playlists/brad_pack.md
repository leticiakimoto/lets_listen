---
title: "Brad_pack"
date: 2022-10-09T16:41:40-03:00
draft: false
---

# Brat Pack

![The Breakfast Club](https://www.looper.com/img/gallery/what-the-brat-pack-looks-like-today/intro.jpg?width=400) 

### Brat Pack é o nome que era dado a um grupo de atores de filmes adolescentes dos anos 80. Essa playlist trago algumas músicas desses filmes e que nos remetem a essa juventude.

## 1) [Don’t You (Forget About Me)](https://www.youtube.com/watch?v=CdqoNKCCt7A)
### Simple Minds

## 2) [Forever Young](https://www.youtube.com/watch?v=t1TcDHrkQYg)
### Alphaville

## 3) [If You Leave](https://www.youtube.com/watch?v=EPmTGFg06zA)
### Orchestral Manoeuvres in the Dark

## 4) [Being Boring](https://www.youtube.com/watch?v=DnvFOaBoieE)
### Pet Shop Boys

## 5) [Pretty In Pink](https://www.youtube.com/watch?v=fqe_XtjYhcg)
### The Psychedelic Furs

## 6) [St. Elmos Fire](https://www.youtube.com/watch?v=dx7vNdAb5e4)
### John Parr

## 7) [Take On Me](https://www.youtube.com/watch?v=djV11Xbc914)
### A-ha

## 8) [In Your Eyes](https://www.youtube.com/watch?v=kU8OJAOMbPg)
### Peter Gabriel

## 9) [Heroes](https://www.youtube.com/watch?v=lXgkuM2NhYI)
### David Bowie

## 10) [Everybody Wants To Rule The World](https://www.youtube.com/watch?v=aGCdLKXNF3w)
### Tears For Fears