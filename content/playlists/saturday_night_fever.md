---
title: "Saturday_night_fever"
date: 2022-10-09T16:41:14-03:00
draft: false
---

# Saturday Night Fever

![Saturday Night Fever](https://cinemaedebate.files.wordpress.com/2011/09/os-embalos-de-sc3a1bado-c3a0-noite.jpg) 

### Músicas para dançar como se estivesse em uma balada disco!

## 1) [Stayin’ Alive](https://www.youtube.com/watch?v=fNFzfwLM72c)
### Bee Gees

## 2) [Dancing Queen](https://www.youtube.com/watch?v=xFrGuyw1V8s)
### ABBA

## 3) [Celebration](https://www.youtube.com/watch?v=3GwjfUFyY6M)
### Kool & The Gang

## 4) [September](https://www.youtube.com/watch?v=Gs069dndIYk)
### Earth, Wind & Fire

## 5) [Waterloo](https://www.youtube.com/watch?v=Sj_9CiNkkn4)
### ABBA

## 6) [Gimme! Gimme! Gimme!](https://www.youtube.com/watch?v=XEjLoHdbVeE)
### ABBA

## 7) [More Than A Woman](https://www.youtube.com/watch?v=uQ9_MwZIaoc)
### Bee Gees

## 8) [Funkytown](https://www.youtube.com/watch?v=jPF59O7_qw0)
### Lipps, Inc.

## 9) [It's Raining Men](https://www.youtube.com/watch?v=l5aZJBLAu1E)
### The Weather Girls

## 10) [Mamma Mia!](https://www.youtube.com/watch?v=unfzfe8f9NI)
### ABBA